package levan.bujiashvili.davaleba1.repository;

import levan.bujiashvili.davaleba1.model.Tag;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TagRepository extends JpaRepository<Tag,Long> {
}
