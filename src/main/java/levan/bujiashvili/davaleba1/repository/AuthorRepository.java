package levan.bujiashvili.davaleba1.repository;

import levan.bujiashvili.davaleba1.model.Author;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorRepository extends JpaRepository<Author,Long> {
}
