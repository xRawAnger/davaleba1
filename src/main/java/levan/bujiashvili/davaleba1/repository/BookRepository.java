package levan.bujiashvili.davaleba1.repository;

import levan.bujiashvili.davaleba1.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookRepository extends JpaRepository<Book,Long> {
}
